from setuptools import setup

setup(name='prototype',
      version='0.1',
      description='Prototype to demonstrate the fesability of linking Semantic Instance Graphs into a Global Instance Graph in order to detect distributed inference attacks',
      url='http://gitlab.com/plht/prototype',
      author='Paul Lachat',
      author_email='paul.lachat@insa-lyon.fr',
      license='MIT',
      packages=['prototype'],
      zip_safe=False)
