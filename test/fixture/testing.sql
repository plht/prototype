-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
CREATE TABLE `Test1` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Test1` (`id`, `name`) VALUES
(1, 'pouet1'),
(2, 'pouet2'),
(3, 'pouet3'),
(4, 'pouet4');

CREATE TABLE `Test2` (
  `id` int(11) NOT NULL,
  `test1_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `text` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Test2` (`id`, `test1_id`, `name`, `text`) VALUES
(1, 4, 'haha1', 'Horrendous'),
(2, 3, 'haha2', 'Ungfell'),
(3, 2, 'haha3', 'Der Weg einer Freiheit'),
(4, 1, 'haha4', 'Blotted Science');

ALTER TABLE `Test1`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `Test2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `test1_id` (`test1_id`);

ALTER TABLE `Test1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `Test2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `Test2`
  ADD CONSTRAINT `test2_ibfk_1` FOREIGN KEY (`test1_id`) REFERENCES `Test1` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
