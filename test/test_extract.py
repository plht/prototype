# -*- coding: utf-8 -*-

import sys
import json
import unittest
from pathlib import Path

import MySQLdb

from prototype import extract

class ExtractTest(unittest.TestCase):

    args = {'host': 'mysql', 'database': 'testing', 'user': 'root', 'password': 'test'}

    query_wo_arg = "SELECT DATABASE()"
    query_wi_arg = "SELECT * FROM information_schema.schemata WHERE SCHEMA_NAME = %s"
    model = {
        'name': args['database'],
        'tables': {
            'Test1': {'pk': 'Test1.id', 'fk': []},
            'Test2': {'pk': 'Test2.id', 'fk': ['Test2.test1_id']}
        },
        'dependencies': [
            {
                'from': {'attribute': 'Test1.name', 'table': 'Test1'},
                'to': {'attribute': 'Test2.name', 'table': 'Test2'},
                'tables': ['Test1', 'Test2'],
                'access': [['Test1.id', 'Test2.test1_id']]
            },
            {
                'from': {'attribute': 'Test1.name', 'table': 'Test1'},
                'to': {'attribute': 'Test2.text', 'table': 'Test2'},
                'tables': ['Test1', 'Test2'],
                'access': [['Test1.id', 'Test2.test1_id']]
            }
        ]
    }

    instances_pk_expected = {'Test1': {1, 2, 3, 4}, 'Test2': {1, 2, 3, 4}}

    related_instances_pk_expected = [
            (('Test1.name', 'Test2.name'), (1, 4), (2, 3), (3, 2), (4, 1)),
            (('Test1.name', 'Test2.text'), (1, 4), (2, 3), (3, 2), (4, 1))
    ]

    instances_values_expected = {
                'Test1': (
                    ('id', 'name'),
                    (1, 'pouet1'),
                    (2, 'pouet2'),
                    (3, 'pouet3'),
                    (4, 'pouet4')),
                'Test2': (
                    ('id', 'test1_id', 'name', 'text'),
                    (1, 4, 'haha1', 'Horrendous'),
                    (2, 3, 'haha2', 'Ungfell'),
                    (3, 2, 'haha3', 'Der Weg einer Freiheit'),
                    (4, 1, 'haha4', 'Blotted Science'))
    }

    model_file = Path('test/fixture/model.json')

    @classmethod
    def setUpClass(cls):
        with open('test/fixture/testing.sql', 'r') as f:
            schema = f.read()
        try:
            extract.execute_query(cls.args, schema)
        except MySQLdb.Error:
            pass

        with open(cls.model_file, 'w') as f:
            json.dump(cls.model, f)

    @classmethod
    def tearDownClass(cls):
        query_drop = "DROP TABLE Test2, Test1"
        try:
            extract.execute_query(cls.args, query_drop)
        except MySQLdb.Error:
            pass

        cls.model_file.unlink()

    def test_build_query_dependencies_related_instances(self):
        link = {
            'access': [
                ['Test1', 'Test2'],
                ['Test3', 'Test4']
            ],
            'tables': ['Pouet']
        }
        pks = {'Attr1': 'TableX', 'Attr2': 'TableZ'}
        expected = "SELECT Attr1, Attr2 FROM Pouet WHERE Test1 = Test2 AND Test3 = Test4"
        result = extract.build_query_dependencies_related_instances(link, pks)
        self.assertEqual(expected, result)

    def test_build_query_values_instances(self):
        table = 'Pouet'
        pk = 'Pouet.pouet_id'
        pks = (1, 2, 3, 4)
        expected = "SELECT * FROM Pouet WHERE Pouet.pouet_id IN (%s, %s, %s, %s)"
        result = extract.build_query_values_instances(table, pk, pks)
        self.assertEqual(expected, result)

    def test_build_query_show_columns(self):
        table = 'Pouet'
        expected = 'SHOW COLUMNS FROM Pouet'
        result = extract.build_query_show_columns(table)
        self.assertEqual(expected, result)

    def test_execute_query_no_values(self):
        expected = self.args['database']
        result = extract.execute_query(self.args, self.query_wo_arg)
        self.assertEqual((expected,), result[0])

    def test_execute_query_cursor_type(self):
        result = extract.execute_query(self.args, self.query_wo_arg, cursor_type=MySQLdb.cursors.DictCursor)
        self.assertTrue('DATABASE()' in result[0])

    def test_execute_query_values(self):
        expected = ('def', 'testing', 'latin1', 'latin1_swedish_ci', None)
        result = extract.execute_query(self.args, self.query_wi_arg, ((self.args['database'],),))
        self.assertEqual(expected, result[0])

    def test_execute_query_bad_connection(self):
        args = dict(self.args)
        args['database'] = 'pouet'
        with self.assertRaises(MySQLdb.Error):
            extract.execute_query(args, self.query_wo_arg)

    def test_execute_query_bad_query(self):
        query = "SELECT *"
        with self.assertRaises(MySQLdb.Error):
            extract.execute_query(self.args, query)

    def test_execute_query_bad_result(self):
        query_no_result = "SELECT * FROM Test1 WHERE id = 10"
        with self.assertRaises(MySQLdb.Error):
            extract.execute_query(self.args, query_no_result)

    def test_fetch_related_instances_pk(self):
        ipk, ripk = extract.fetch_related_instances_pk(self.args, self.model)

        self.assertEqual(self.instances_pk_expected, ipk)
        self.assertEqual(self.related_instances_pk_expected, ripk)

    def test_fetch_columns_fields(self):
        expected = ('id', 'name')
        result = extract.fetch_columns_fields(self.args, 'Test1')
        self.assertEqual(expected, result)

        expected = ('id', 'test1_id', 'name', 'text')
        result = extract.fetch_columns_fields(self.args, 'Test2')
        self.assertEqual(expected, result)
    
    def test_fetch_related_instances_values(self):
        result = extract.fetch_related_instances_values(self.args, self.model, self.instances_pk_expected)

        self.assertEqual(self.instances_values_expected, result)
    def test_extract_instances(self):
        model_file = str(self.model_file)
        instances_values_result, related_instances_pk_result = extract.extract_instances(model_file, self.args['host'], self.args['database'], self.args['user'], self.args['password'])

        self.assertEqual(self.instances_values_expected, instances_values_result)
        self.assertEqual(self.related_instances_pk_expected, related_instances_pk_result)

    def test_build_pairs_normal(self):
        attrs = [
                'test1@test1@1.dat',
                'test2@test1@1.dat',
                'test1@test2@2.txt'
        ]

        expected = [
                ({'bdd': 'sakila', 'table': 'Test1', 'attr': 'test1'}, {'bdd': 'northwind', 'table': 'Test2', 'attr': 'test1'}),
                ({'bdd': 'sakila', 'table': 'Test1', 'attr': 'test2'}, {'bdd': 'northwind', 'table': 'Test2', 'attr': 'test1'}),
        ]

        result = []
        extract.build_pairs(attrs, result)
        self.assertEqual(expected, result)

    def test_build_pairs_customer(self):
        attrs = [
                'test1@address@1.dat',
                'test1@city@1.dat',
                'test1@country@2.txt'
        ]

        expected = [
                ({'bdd': 'sakila', 'table': 'Customer', 'attr': 'test1'}, {'bdd': 'northwind', 'table': 'Customer', 'attr': 'test1'}),
                ({'bdd': 'sakila', 'table': 'Customer', 'attr': 'test1'}, {'bdd': 'northwind', 'table': 'Customer', 'attr': 'test1'})
        ]

        result = []
        extract.build_pairs(attrs, result)
        self.assertEqual(expected, result)
