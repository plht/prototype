# Semantic Instance Graph (SIG)

A SIG represent the dependencies of a database at the instance level. It is obtained by instanciating a [SIM](../model/README.md) with the instances present in the related database. The SIG have been proposed by [Chen, Y., & Chu, W. W. (2006, May)](https://www.researchgate.net/profile/Wesley_Chu/publication/243135169_Database_Security_Protection_Via_Inference_Detection/links/543580c30cf2643ab986799c/Database-Security-Protection-Via-Inference-Detection.pdf).

# Global Instance Graph (GIG)

The GIG is specific to our solution and is computed by linking together the SIGs of databases based on the similar instances. This graph is used to detect inference attacks distributed on the related dabatases.

# JSON graphs representation

In this project, both the SIG and GIG are represented as .json files and both called `graph` in the source code.

The file `sig_schema.json` and `gig_schema.json` define the schema of the .json files representing, respectively, SIG and GIG using the vocabulary of [JSON Schema](https://json-schema.org/).

A tool such as [jsonschema](https://github.com/Julian/jsonschema) can be used to validate a model with the schema. For example: `jsonschema sig_schema.json -i 1/graph.json` where `graph.json` is the SIG to validate or `jsonschema gig_schema.json -i 1/global_graph.json` for a GIG.

