# Preamble

We assume that a MySQL database management system is running on your system and that the two databases of the dataset are hosted in the same system and can be accessed by the same user. We use `phphMyAdmin 4.6.6` with the default configuration (i.e., `localhost:3306`).

We also assume that the Python requirements are installed. If not, run the following command `pip3 install -r requirements.txt`.

Eventually, we assume that all the command are run in a CLI from the root of the project.

## Basic interaction to compute the Global Instance Graph (GIG)

You can find the detailled explanation of each sub-command in the [README.md](../README.md) at the root of the project.

We assume that the [Northix](https://archive.ics.uci.edu/ml/datasets/Northix) dataset root file as been download into [dataset/](../dataset).

The first step is then to load the two databases into the MySQL DBMS by running the following command:

`python3 -m prototype load dataset/Northix localhost root test sakila dataset/sakila.sql northwind dataset/northwind.sql`

Which display the following information:

```
[*] Connection to localhost with user: root and password: test
[*] Create database: sakila
[*] Connection to localhost/sakila with user: root and password: test
[*] Loading database schema from dataset/sakila.sql ...
[*] Connection to localhost with user: root and password: test
[*] Create database: northwind
[*] Connection to localhost/northwind with user: root and password: test
[*] Loading database schema from dataset/northwind.sql ...
[*] 115 files found in /home/zar/Documents/m2_recherche/donnee/Northix/InputData
[*] Connection to localhost/sakila with user: root and password: test
[*] Insert instances into the tables:
    - actor
    - customer
    - film
    - film_actor
    - inventory
    - rental
    - payment
[*] Insertion done !
[*] Connection to localhost/northwind with user: root and password: test
[*] Insert instances into the tables:
    - customers
    - suppliers
    - products
    - orders
    - order_details
[*] Insertion done !
```

Since the Northix dataset does not contains similar instances natively, we must artificialy create duplicate of similar instances:

`python3 -m prototype duplicate localhost sakila northwind root test model/1/sakila.json model/1/northwind.json 4 sakila`

The customer instances to duplicate are selected randomly, thus you can have a different display.

```
[*] Duplication of 4 customer(s) from sakila to northwind successed !
    Primary keys of the customer in sakila:
      - Customer     : [5, 2, 1, 10]
    Primary keys of the new instances in northwind:
      - Customers    : [114, 115, 116, 117]
      - Orders       : [10630, 10631, 10632, 10633]
      - Order_Details: [1008, 1009, 1010, 1011]
```

Now that the dataset is setup, we must play the role of the data controller and build their respective information.
The first data controller use his Semantic Inference Model (SIM) `model/1/sakila.json` to compute the anonymised Semantic Instance Graph (SIG) and the related Bloom Filters:

`python3 -m prototype build model/1/sakila.json localhost sakila root test graph/sakila.json`

```
[*] Use the SIM defined in model/1/sakila.json
[*] Extract related instances from the database localhost/sakila with user: root and password: test
[*] Anonymise the extracted instances and compute the Bloom Filters
[*] Build the SIG
[*] File successfully created in graph/sakila.json
```

The second data controller proceed to the same computation related to is own SIM.

`python3 -m prototype build model/1/northwind.json localhost northwind root test graph/northwind.json`

```
[*] Use the SIM defined in model/1/northwind.json
[*] Extract related instances from the database localhost/northwind with user: root and password: test
[*] Anonymise the extracted instances and compute the Bloom Filters
[*] Build the SIG
[*] File successfully created in graph/northwind.json
```

The last information missing to be able to compute the GIG is the schema matching between the SIMs of the both data controllers.
Hopefully, this information is provided by the Northix dataset:

`python3 -m prototype extract_matching dataset/Northix model/1/schema_matching.json`

`[*] The file model/1/schema_matching.json as been successfully created.`

Now we have all the input required to compute the GIG:

`python3 -m prototype link model/1/schema_matching.json model/1/sakila.json graph/sakila.json model/1/northwind.json graph/northwind.json graph/global_graph.json`

`[*] The file graph/global_graph.json as been successfully created.`

Finally, we can visualise the computed GIG:

`python3 -m prototype visualise graph/global_graph.json model/1/sakila.json model/1/northwind.json`

`[*] Please wait ...`

Which produce this kind of picture:

![Example of a Global Instance Graph](example_gig.png)

Note that since the duplicated customer instances are chosen randomly and that the networkx library organise the nodes of the GIG itself, the resulting picture can have a different layout that the one display in this tutorial.

# Reproduce the measures

Using the benchmark programm is straightforward. First be sure to follow the [preamble](#preamble).

## Parameters

Here are the values of the parameters used for running the benchmark:

- Number of pairs of similar instances insterted at each step: `100`
- Maxium of pairs inserted: `10000`
- Number of GIG computation repetition for each step: `10`

## Benchmark

Then you can run the following command:

`python3 -m prototype benchmark dataset/Northix localhost root test sakila dataset/sakila.sql northwind dataset/northwind.sql model/1/sakila.json model/1/northwind.json graph/sakila.json graph/northwind.json graph/global_graph.json model/1/schema_matching.json 100 10000 10 ./results.csv`

## Docker containers

To run the benchmark of the GIG computation, we used Docker containers. Thus, we describe how to setup these containers.

First, build the image from the project root using the provided [Dockerfile](../Dockerfile):

`docker build -t prototype .`

Then run the MySQL container, which can take some time to setup:

`docker run --name mysql_prototype -p 3306:3306 -e MYSQL_ROOT_PASSWORD=test -d mysql:5.7`

Finally run the prototype container:

`docker run --name main_prototype -it prototype bash`

And run the [benchmark](#command) command from the bash shell and replace `localhost` by the name of the MySQL container `mysql_prototype`.

Quit the container without killing it by typing `ctrl+p` followed by `ctrl+q`.
