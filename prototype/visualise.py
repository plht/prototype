# -*- coding: utf-8 -*-
#
# Visualise a global graph

import json

import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.colors as cls
from matplotlib.lines import Line2D
import seaborn as sns

def get_dependency_related_colors(model1_file, model2_file):
    """Map colors to dependencies"""
    with open(model1_file, 'r') as f:
        model1 = json.load(f)

    with open(model2_file, 'r') as f:
        model2 = json.load(f)

    dependency_color = {}
    nbr_color = len(model1['dependencies']) + len(model2['dependencies'])
    light_palette = list(map(cls.to_hex, sns.hls_palette(nbr_color, l=0.5, s=0.8)))
    dark_palette = list(map(cls.to_hex, sns.hls_palette(nbr_color, l=0.3, s=0.8)))

    for link in model1['dependencies']:
        key = (link['from']['attribute'], link['to']['attribute'], model1['name'])
        dependency_color[key] = (light_palette.pop(), dark_palette.pop())
    for link in model2['dependencies']:
        key = (link['from']['attribute'], link['to']['attribute'], model2['name'])
        dependency_color[key] = (light_palette.pop(), dark_palette.pop())
    return dependency_color

def get_nodes_dependency_related_color(graph, dependency_color):
    """Get dependency related color for each nodes"""
    nodes = {}
    for l_col, d_col in dependency_color.values():
        nodes[l_col], nodes[d_col] = [], []
    for edge in graph['edges']:
        for table, pk, attribute in edge.values():
            key_part = '.'.join((table, attribute))
            for key in dependency_color.keys():
                if key_part == key[0]:
                    color = dependency_color[key][0]
                if key_part == key[1]:
                    color = dependency_color[key][1]
            node = '.'.join((table, pk, attribute))
            nodes[color].append(node)
    # Remove duplicate nodes
    return {k: set(v) for k, v in nodes.items()}

def get_edges(graph):
    """Get the edges of the global graph"""
    edges = []
    for edge in graph['edges']:
        current_edge = tuple(map('.'.join, edge.values()))
        edges.append(current_edge)
    return edges

def build_graph(graph):
    """Build the global graph from a JSON file"""
    edges = get_edges(graph)
    matches = [tuple(map('.'.join, match.values())) for match in graph['matches']]

    global_graph = nx.Graph()
    global_graph.add_edges_from(edges)
    global_graph.add_edges_from(matches)

    return global_graph, edges, matches

def visualise_global_graph_dependency_related_color(global_graph_file, model1_file, model2_file):
    """Visualise the global graph where each graphs have a color related to their dependency"""
    with open(global_graph_file, 'r') as f:
        graph = json.load(f)

    global_graph, edges, matches = build_graph(graph)

    dependency_color = get_dependency_related_colors(model1_file, model2_file)
    dl_color = 'orange'
    si_color = 'red'

    nodes = get_nodes_dependency_related_color(graph, dependency_color)
    _, ax = plt.subplots()

    legend_elements = []
    # Left column of the legend
    for (label1, _, db), (l_color, _) in dependency_color.items():
        legend_elements.append(Line2D([0], [0], marker='o', color='w', markerfacecolor=l_color, label='{:s}.{:s}'.format(db, label1)))
    legend_elements.append(Line2D([0], [0], color=dl_color, label='Dependency link'))

    # Right column of the legend
    for (_, label2, db), (_, d_color) in dependency_color.items():
        legend_elements.append(Line2D([0], [0], marker='o', color='w', markerfacecolor=d_color, label='{:s}.{:s}'.format(db, label2)))
    legend_elements.append(Line2D([0], [0], color=si_color, label='Similarity link'))

    ax.legend(handles=legend_elements, ncol=2)

    pos = nx.spring_layout(global_graph)
    # Draw nodes with different using the color related to the dependencies
    for color, nodes in nodes.items():
        nx.draw_networkx_nodes(global_graph, pos, nodelist=nodes, node_color=color, node_size=10, ax=ax)

    # Draw the dependency and matching edges
    nx.draw_networkx_edges(global_graph, pos, edgelist=edges, width=1, edge_color=dl_color, ax=ax)
    nx.draw_networkx_edges(global_graph, pos, edgelist=matches, width=1, edge_color=si_color, ax=ax)

    plt.show()
