# -*- coding: utf-8 -*-
#
# Build the graphs of a database

import json

def build_nodes(anonymised_data):
    """Build nodes of the graph"""
    nodes_per_table = {}
    for table, insts in anonymised_data.items():
        current_table = {}
        for pk, bf_vals in insts.items():
            if pk is 'attributes':
                continue
            nodes = {}
            for value, name in zip(bf_vals['values'], insts['attributes']):
                nodes[name] = value
            current_table[pk] = {'bfs': bf_vals['bfs'], 'nodes': nodes}
        nodes_per_table[table] = current_table
    return nodes_per_table

def build_edges(pk_related_instances):
    """Build edges of the graph"""
    edges = []
    for dep in pk_related_instances:
        table_from, attr_from, table_to, attr_to = '.'.join(dep[0]).split('.')
        for pk_from, pk_to in dep[1:]:
            edges.append(((table_from, str(pk_from), attr_from), (table_to, str(pk_to), attr_to)))
    edges = set(edges)
    return [{'from': edge[0], 'to': edge[1]} for edge in edges]

def build_graph(db_name, pk_related_instances, anonymised_data):
    """Build the graph"""
    nodes_per_table = build_nodes(anonymised_data)
    edges = build_edges(pk_related_instances)
    return {'name': db_name, 'tables': nodes_per_table, 'edges': edges}
