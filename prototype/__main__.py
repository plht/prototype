# -*- coding: utf-8 -*-

import os
import sys
import json
import argparse

import MySQLdb

from prototype import load as ld
from prototype import extract
from prototype import anonymise
from prototype import build as bl
from prototype import duplicate as dp
try:
    from prototype import visualise as vs
except ImportError:
    pass
from prototype import link as lk
from prototype import benchmark as bc

def check_files_valid(files):
    for fl in files:
        if not os.path.isfile(fl):
            print("[!] {:s} is not a file or does not exist.".format(fl), file=sys.stderr)
            sys.exit(2)

def check_dirs_valid(dirs):
    for dr in dirs:
        if not os.path.isdir(dr):
            print("[!] {:s} is not a directory or does not exists".format(dr), file=sys.stderr)
            sys.exit(2)

def get_Northix_input_data_path(dr):
    root = os.path.abspath(dr)
    return os.path.join(root, 'InputData')

def get_Northix_mapping_path(dr):
    root = os.path.abspath(dr)
    return os.path.join(dr, 'Classes')

def load(args):
    """Load the Northix dataset"""
    input_data_path = get_Northix_input_data_path(args.dir) 

    check_dirs_valid([input_data_path])
    check_files_valid([args.schema1, args.schema2])

    ld.load_Northix_dataset(input_data_path, args)

def build(args):
    """Build the anonymised Semantic Instance Graph (SIG) and the Bloom Filters related to Semantic Inference Model (SIM) and the instances of a database"""
    check_files_valid([args.model_file])

    msg = "[*] Use the SIM defined in {:s}\n"
    msg += "[*] Extract related instances from the database {:s}/{:s} with user: {:s} and password: {:s}"

    print(msg.format(args.model_file, args.host, args.name, args.user, args.password))
    try:
        prep_insts, pk_rel_insts = extract.extract_instances(args.model_file, args.host, args.name, args.user, args.password)
    except MySQLdb.Error as e:
        sys.exit(2)

    print("[*] Anonymise the extracted instances and compute the Bloom Filters")
    anonymised_instances = anonymise.process_instances(args.model_file, prep_insts)

    print("[*] Build the SIG")
    graphs = bl.build_graph(args.name, pk_rel_insts, anonymised_instances)

    with open(args.output, 'w') as f:
        json.dump(graphs, f)

    print("[*] File successfully created in {:s}".format(args.output))

def duplicate(args):
    """Duplicate Customer instances from one database to the other while respecting the Semantic Inference Model (SIMs) defined"""
    check_files_valid([args.model_file1, args.model_file2])

    db1_args = {'host': args.host, 'database': args.name1, 'user': args.user, 'password': args.password}
    db2_args = {'host': args.host, 'database': args.name2, 'user': args.user, 'password': args.password}
    if args.source == 'sakila':
        dp.duplicate_from_sakila(db1_args, db2_args, args.duplicate, args.model_file1)
    elif args.source == 'northwind':
        dp.duplicate_from_northwind(db1_args, db2_args, args.duplicate, args.model_file2)
    elif args.source == 'both':
        dp.duplicate_from_both(db1_args, db2_args, args.duplicate, args.model_file1, args.model_file2)
    else:
        print("[!] Unkown source choice: {:s} for duplication.".format(args.source), file=sys.stderr)
        sys.exit(2)

def extract_mapping(args):
    """Extract the dependencies of models related to the schema matching"""
    mapping_dir = get_Northix_mapping_path(args.dir)
    check_dirs_valid([mapping_dir])

    extract.extract_dependency_attributes_matching(mapping_dir, args.output)
    print("[*] The file {:s} as been successfully created.".format(args.output))

def visualise(args):
    """Visualise the Global Instance Graph (GIG) based on the Semantic Inference Models (SIMs)"""
    check_files_valid([args.global_graph_file, args.model_file1, args.model_file2])

    print("[*] Please wait ...")
    vs.visualise_global_graph_dependency_related_color(args.global_graph_file, args.model_file1, args.model_file2)

def link(args):
    """Link the Semantic Instance Graphs (SIGs) based on the schema matching of the related Semantic Inference Models (SIMs) in order to computethe Global Instance Graph (GIG)"""
    check_files_valid([args.schema_matching_file, args.model_file1, args.model_file2, args.graph_file1, args.graph_file2])

    lk.generate_global_graph(args.schema_matching_file, args.model_file1, args.model_file2, args.graph_file1, args.graph_file2, args.global_graph_file)
    print("[*] The file {:s} as been successfully created.".format(args.global_graph_file))

def benchmark(args):
    """Measure the grow of the Global Instance Graph (GIG) computation time"""
    check_files_valid([args.model_file1, args.model_file2])

    input_data_path = get_Northix_input_data_path(args.dir) 
    mapping_dir = get_Northix_mapping_path(args.dir)
    check_dirs_valid([input_data_path, mapping_dir])
    
    bc.benchmark(input_data_path, mapping_dir, args)

parser = argparse.ArgumentParser(prog='prototype')
subparsers = parser.add_subparsers(dest='cmd', help='Sub-command choice')

parser_load = subparsers.add_parser('load', help='Load the Northix dataset')
parser_load.add_argument("dir", help="Northix Data Set folder root", type=str)
parser_load.add_argument("host", help="Database hostname", type=str)
parser_load.add_argument("user", help="Database user", type=str)
parser_load.add_argument("password", help="Database password", type=str)
parser_load.add_argument("name1", help="First database name", type=str)
parser_load.add_argument("schema1", help="First database schema", type=str)
parser_load.add_argument("name2", help="Second database name", type=str)
parser_load.add_argument("schema2", help="Second database schema", type=str)
parser_load.set_defaults(func=load)

parser_build = subparsers.add_parser('build', help='Build the anonymised Semantic Instance Graph (SIG) and the Bloom Filters related to Semantic Inference Model (SIM) and the instances of a database')
parser_build.add_argument("model_file", help="SIM as a .json", type=str)
parser_build.add_argument("host", help="Database hostname", type=str)
parser_build.add_argument("name", help="Database name", type=str)
parser_build.add_argument("user", help="Database user", type=str)
parser_build.add_argument("password", help="Database password", type=str)
parser_build.add_argument("output", help="Save the SIG as a .json", type=str)
parser_build.set_defaults(func=build)

parser_duplicate = subparsers.add_parser('duplicate', help='Duplicate Customer instances from one database to the other while respecting the Semantic Inference Model (SIMs) defined')
parser_duplicate.add_argument("host", help="Database hostname", type=str)
parser_duplicate.add_argument("name1", help="First database name", type=str)
parser_duplicate.add_argument("name2", help="Second database name", type=str)
parser_duplicate.add_argument("user", help="Database user", type=str)
parser_duplicate.add_argument("password", help="Database password", type=str)
parser_duplicate.add_argument("model_file1", help="SIM of the first database", type=str)
parser_duplicate.add_argument("model_file2", help="SIM of the second database", type=str)
parser_duplicate.add_argument("duplicate", help="Number of customers to duplicate", type=int)
parser_duplicate.add_argument("source", choices=('sakila', 'northwind', 'both'), help="Source database of the customers to duplicate", type=str)
parser_duplicate.set_defaults(func=duplicate)

parser_extract_matching = subparsers.add_parser('extract_matching', help='Extract the schema matching from the Northix dataset')
parser_extract_matching.add_argument("dir", help="Northix Data Set folder root", type=str)
parser_extract_matching.add_argument("output", help="Save the schema matching as a .json", type=str)
parser_extract_matching.set_defaults(func=extract_mapping)

parser_visualise = subparsers.add_parser('visualise', help='Visualise the Global Instance Graph (GIG) based on the Semantic Inference Models (SIMs)')
parser_visualise.add_argument("global_graph_file", help="GIG file", type=str)
parser_visualise.add_argument("model_file1", help="SIM of the first database as a .json", type=str)
parser_visualise.add_argument("model_file2", help="SIM of the second database as a .json", type=str)
parser_visualise.set_defaults(func=visualise)

parser_link = subparsers.add_parser('link', help='Link the Semantic Instance Graphs (SIGs) based on the schema matching of the related Semantic Inference Models (SIMs) in order to computethe Global Instance Graph (GIG)')
parser_link.add_argument("schema_matching_file", help="Schema matching as a .json", type=str)
parser_link.add_argument("model_file1", help="SIM of the first database as a .json", type=str)
parser_link.add_argument("model_file2", help="SIM of the second database as a .json", type=str)
parser_link.add_argument("graph_file1", help="SIG of the first database as a .json", type=str)
parser_link.add_argument("graph_file2", help="SIG of the second database as a .json", type=str)
parser_link.add_argument("global_graph_file", help="Output file for the GIG as a .json", type=str)
parser_link.set_defaults(func=link)

parser_benchmark = subparsers.add_parser('benchmark', help='Measure the grow of the Global Instance Graph (GIG) computation time')
parser_benchmark.add_argument("dir", help="Northix Data Set folder root", type=str)
parser_benchmark.add_argument("host", help="Database hostname", type=str)
parser_benchmark.add_argument("user", help="Database user", type=str)
parser_benchmark.add_argument("password", help="Database password", type=str)
parser_benchmark.add_argument("name1", help="First database name", type=str)
parser_benchmark.add_argument("schema1", help="First database schema", type=str)
parser_benchmark.add_argument("name2", help="Second database name", type=str)
parser_benchmark.add_argument("schema2", help="Second database schema", type=str)
parser_benchmark.add_argument("model_file1", help="SIM of the first database as a .json", type=str)
parser_benchmark.add_argument("model_file2", help="SIM of the second database as a .json", type=str)
parser_benchmark.add_argument("graph_file1", help="SIG of the first database as a .json", type=str)
parser_benchmark.add_argument("graph_file2", help="SIG of the second database as a .json", type=str)
parser_benchmark.add_argument("global_graph_file", help="GIG as a .json", type=str)
parser_benchmark.add_argument("schema_matching_file", help="Schema matching as a .json", type=str)
parser_benchmark.add_argument("step_nbr", help="Number similar Customer instances couples to insert at each step", type=int)
parser_benchmark.add_argument("limit_duplicate_nbr", help="Number limit of Customer instances couples to insert", type=int)
parser_benchmark.add_argument("exec_nbr", help="Number of execution for each step", type=int)
parser_benchmark.add_argument("output_file", help="Output file in which measures are saved", type=str)
parser_benchmark.add_argument('--raw-output', help="Define if measures must be write to a file or directly to stdout", dest='raw_output', action='store_true')
parser_benchmark.set_defaults(func=benchmark, raw_output=False)

args = parser.parse_args()
# Display usage when no sub-command is given
if not args.cmd:
    parser.parse_args(['-h'])
else:
    args.func(args)
