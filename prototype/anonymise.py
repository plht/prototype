# -*- coding: utf-8 -*-
#
# Anonymise instances and compute their Bloom Filter

import json
import nltk
import base64
import hashlib
import concurrent.futures

from bitarray import bitarray

def serialise_bloom_filter(bloom_filter):
    """Serialise the bloom filter into a JSON friendly format"""
    return base64.b64encode(bloom_filter.tobytes()).decode('utf-8')

def hash_data(data, as_hex=False):
    """Hash attribute or bigrams using the Blake2b hash function"""
    data_hash = hashlib.blake2b(data.encode('utf-8'))
    if as_hex:
        return data_hash.hexdigest()
    else:
        return data_hash.digest()

def encode_attribute(bloom_filter, attr):
    """Encode an instance into a Bloom Filter"""
    length = bloom_filter.length()
    attr_bigrams = nltk.bigrams("_{:s}_".format(attr))
    for bg in attr_bigrams:
        b = ''.join(bg)
        h = hash_data(b)
        m = int.from_bytes(h, 'big') % length
        bloom_filter[m] = 1

def process_instance(instance, indexs):
    """Anonise instance and create its Bloom Filter"""
    hash_inst = []
    bloom_filter = bitarray('0' * 1024)
    length = len(instance)
    for i, attr in enumerate(instance):
        attr_str = str(attr)
        encode_attribute(bloom_filter, attr_str)
        if i in indexs:
            hash_attr = hash_data(attr_str, as_hex=True)
            hash_inst.append(hash_attr)
    ser_bf_inst = serialise_bloom_filter(bloom_filter)
    return (hash_inst, ser_bf_inst)

def concurrently_process_instances(instances, pk_instance, indexs):
    """Concurrently anonymise instances"""
    processed_instances = {}
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(instances)) as executor:
        futures = {}
        for pk, inst in zip(pk_instance, instances):
            key = executor.submit(process_instance, inst, indexs)
            futures[key] = pk

        for future in concurrent.futures.as_completed(futures):
            try:
                anon_inst, bf_inst = future.result()
            except Exception as e:
                print("[!] Error while retrieving the anonymised instance: {}.".format(e))
            else:
                pk = futures[future]
                processed_instances[pk] = {'bfs': bf_inst, 'values': anon_inst}
    return processed_instances

def get_index_attributes_to_hash(model, table, attributes_names):
    """Get the indexs of the attributes related to a dependencies"""
    indexs = []
    for link in model['dependencies']:
        for key in ['from', 'to']:
            end = link[key]
            if end['table'] == table:
                attr = end['attribute'].split('.')[1]
                index = attributes_names.index(attr)
                indexs.append(index)
    return indexs

def process_instances(model_file, prepared_instances):
    """Compute the hash and the Bloom Filter of each instances"""
    with open(model_file, 'r') as f:
        model = json.load(f)

    processed_instances = {}
    for table, instances in prepared_instances.items():
        attributes_names = instances[0]
        # Preprocess all the instances to encode into Bloom Filter
        # attributes excepted primary and foreign keys and to
        # hash only values related to dependencies
        pk_instance = model['tables'][table]['pk'].split('.')[1]
        fk_instance = [fk.split('.')[1] for fk in model['tables'][table]['fk']]
        pk_fk = [pk_instance] + fk_instance
        attributes_names_wo_pk_fk = [attr for attr in attributes_names if attr not in pk_fk]
        index_pk = attributes_names.index(pk_instance)
        instances_pks = [inst[index_pk] for inst in instances]
        indexs_pk_fk = [attributes_names.index(fk) for fk in fk_instance] + [index_pk]
        instances_wo_pk_fk = []
        for instance in instances:
            length = len(instance)
            instance_wo_pk_fk = []
            for i in range(length):
                if i not in indexs_pk_fk:
                    instance_wo_pk_fk.append(instance[i])
            instances_wo_pk_fk.append(instance_wo_pk_fk)

        indexs = get_index_attributes_to_hash(model, table, instances_wo_pk_fk[0])
        processed_instances[table] = concurrently_process_instances(instances_wo_pk_fk[1:], instances_pks[1:], indexs)
        processed_instances[table]['attributes'] = [attr for i, attr in enumerate(attributes_names_wo_pk_fk) if i in indexs]

    return processed_instances
