# -*- coding: utf-8 -*-
#
# Load data of the Northix Data Set in two databases

import os
import re
import sys
from datetime import datetime

import MySQLdb

def format_date(date):
    """Format dates following the Y-m-d pattern"""
    try:
        d = datetime.strptime(date, '%m/%d/%Y')
    except ValueError:
        d = datetime.strptime(date, '%Y/%m/%d')
    return d.strftime('%Y-%m-%d')

def extract_attribute_values(files, input_data_path, db_ext, badly_formated, file_enc='utf-8'):
    """Extract and format values from the input files"""
    data = {}
    regex = r'^.*@{:s}$'.format(db_ext)
    files_re = re.compile(regex)
    db_files = filter(files_re.match, files)
    attributes_to_ignore = ['address_id', 'city_id', 'country_id', 'last_update']
    tables_to_merge = ['address', 'city', 'country']
    for file_name in db_files:
        attribute, table, db = file_name.split('@')
        path_full_name = os.path.join(input_data_path, file_name)
        if attribute in attributes_to_ignore:
            continue

        with open(path_full_name, 'r', encoding=file_enc) as f:
            current_data = [value if value.strip() != '' else None for value in f.read().splitlines()]
        if attribute in badly_formated:
            current_data = list(map(format_date, current_data))

        if table in tables_to_merge and db_ext == '1.dat':
            if 'customer' not in data:
                data['customer'] = {}
            data['customer'][attribute] = current_data
        else:
            if table not in data:
                data[table] = {}
            data[table][attribute] = current_data
    return data

def create_database(host, name, user, password):
    """Create a database"""
    try:
        print("[*] Connection to {:s} with user: {:s} and password: {:s}".format(host, user, password))
        db = MySQLdb.connect(host=host, user=user, password=password)
        cursor = db.cursor()
    except MySQLdb.Error as e:
        print("[!] Error while connecting to the host: {}".format(e))
        sys.exit(2)

    ok = True
    query_create_database = "CREATE DATABASE {:s} COLLATE utf8mb4_bin".format(name)
    print("[*] Create database: {:s}".format(name))
    try:
        cursor.execute(query_create_database)
    except MySQLdb.Error as e:
        print("[!] Error while creating database: {}\n    Query: {:s}".format(e, query_create_database), file=sys.stderr)
        ok = False
    finally:
        cursor.close()
        db.close()
        if not ok:
            sys.exit(2)

def load_schema(host, name, user, password, schema_file):
    """Load the schema of a database"""
    try:
        print("[*] Connection to {:s}/{:s} with user: {:s} and password: {:s}".format(host, name, user, password))
        db = MySQLdb.connect(host=host, database=name, user=user, password=password)
        cursor = db.cursor()
    except MySQLdb.Error as e:
        print("[!] Error while connecting to the database: {}".format(e))
        sys.exit(2)

    with open(schema_file, 'r') as f:
        content = f.read()

    ok = True
    try:
        print("[*] Loading database schema from {:s} ...".format(schema_file))
        cursor.execute(content)
    except MySQLdb.Error as e:
        print("[!] Error while inserting data: {}".format(e), file=sys.stderr)
        ok = False
    finally:
        cursor.close()
        db.close()
        if not ok:
            sys.exit(2)

def load_data(host, name, user, password, data, table_order):
    """Load the extracted values into a database"""
    try:
        print("[*] Connection to {:s}/{:s} with user: {:s} and password: {:s}".format(host, name, user, password))
        db = MySQLdb.connect(host=host, database=name, user=user, password=password)
        cursor = db.cursor()
    except MySQLdb.Error as e:
        print("[!] Error while connecting to the database: {}".format(e))
        sys.exit(2)

    query = "INSERT IGNORE INTO {:s} ({:s}) VALUES ({:s})"
    try:
        print("[*] Insert instances into the tables:")
        for table in table_order:
            print("    - {:s}".format(table))
            lowercase_attributes = map(lambda attr: attr[0].lower() + attr[1:], data[table].keys())
            current_query = query.format(table.title(), ','.join(lowercase_attributes), ','.join(['%s'] * len(data[table])))
            values = [value for value in zip(*data[table].values())]
            cursor.executemany(current_query, values)
            db.commit()
        print("[*] Insertion done !")
    except MySQLdb.Error as e:
        db.rollback()
        print("[!] Error while inserting data: {}".format(e), file=sys.stderr)
    finally:
        cursor.close()
        db.close()

def load_Northix_dataset(input_data_path, args):
    """Load the Northix dataset in two databases"""

    create_database(args.host, args.name1, args.user, args.password)
    load_schema(args.host, args.name1, args.user, args.password, args.schema1)

    create_database(args.host, args.name2, args.user, args.password)
    load_schema(args.host, args.name2, args.user, args.password, args.schema2)

    files = os.listdir(input_data_path)
    print("[*] {:d} files found in {:s}".format(len(files), input_data_path))

    db1_badly_formated = ['create_date', 'order_dat']
    db1_data = extract_attribute_values(files, input_data_path, '1.dat', db1_badly_formated)

    db2_badly_formated = ['paymentDate', 'registrationDate']
    db2_data = extract_attribute_values(files, input_data_path, '2.txt', db2_badly_formated, file_enc='iso-8859-1')

    # Translate PKs from "varchar(5)" to "int(11)"
    customers_pk_translation = {pk: i for i, pk in enumerate(db2_data['customers']['CustomerID'])}
    db2_data['customers']['CustomerID'] = map(customers_pk_translation.get, db2_data['customers']['CustomerID'])
    db2_data['orders']['CustomerID'] = map(customers_pk_translation.get, db2_data['orders']['CustomerID'])

    db1_table_order = ['actor', 'customer', 'film', 'film_actor', 'inventory', 'rental', 'payment']
    load_data(args.host, args.name1, args.user, args.password, db1_data, db1_table_order)

    db2_table_order = ['customers', 'suppliers', 'products', 'orders', 'order_details']
    load_data(args.host, args.name2, args.user, args.password, db2_data, db2_table_order)
