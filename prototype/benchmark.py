# -*- coding: utf-8 -*-
#
# Measure the grow of the Global Instance Graph (GIG) time computation

import json
import timeit
import subprocess
import MySQLdb

from prototype.load import load_Northix_dataset
from prototype.extract import extract_instances, extract_dependency_attributes_matching
from prototype.anonymise import process_instances
from prototype.build import build_graph
from prototype.link import get_related_instances, create_global_instance_graph
from prototype.duplicate import add_duplicate_Customers_instances

def wrapper(func, *args, **kwargs):
    """Wrap the function to measure"""
    def wrapped():
        return func(*args, **kwargs)
    return wrapped

def build_inputs_and_graph(model_file, db_name, output_graph, args):
    """Build the SIG and Bloom Filters related to a database"""
    prep_insts, pk_rel_insts = extract_instances(model_file, args.host, db_name, args.user, args.password)
    anonymised_instances = process_instances(model_file, prep_insts)
    graph = build_graph(db_name, pk_rel_insts, anonymised_instances)
    with open(output_graph, 'w') as f:
        json.dump(graph, f)

def benchmark(input_data_path, mapping_dir, args):
    # Load the database to start from zero
    load_Northix_dataset(input_data_path, args)

    extract_dependency_attributes_matching(mapping_dir, args.schema_matching_file)

    db1_args = {'host': args.host, 'database': args.name1, 'user': args.user, 'password': args.password}
    db2_args = {'host': args.host, 'database': args.name2, 'user': args.user, 'password': args.password}

    if not args.raw_output:
        with open(args.output_file, 'w') as f:
            f.write("nbr_insts,time\n")
    else:
        print("nbr_insts,time")

    total_duplicate_nbr = 0
    nbr_digit = len(str(abs(total_duplicate_nbr)))
    if not args.raw_output:
        frmt = "\r[.] Total number of similar Customer instance couples: {{:{:d}d}}/{:d}".format(nbr_digit, args.limit_duplicate_nbr)
    while total_duplicate_nbr <= args.limit_duplicate_nbr:

        # Several couple of similar customer instances are inserted in both databases at each step
        try:
            add_duplicate_Customers_instances(args.step_nbr, db1_args, db2_args)
        except MySQLdb._exceptions.DatabaseError as e:
            if not args.raw_output:
                print("[!] Exception related to the duplication of customers instances: {}".format(e))
            continue

        # Computation of the Semantic Instance Graphs and the Bloom Filters needed to compute the GIG
        build_inputs_and_graph(args.model_file1, args.name1, args.graph_file1, args)
        build_inputs_and_graph(args.model_file2, args.name2, args.graph_file2, args)

        # TODO : avoid writting in files
        with open(args.graph_file1, 'r') as f:
            fst_graph = json.load(f)

        with open(args.graph_file2, 'r') as f:
            snd_graph = json.load(f)

        related_instances = get_related_instances(args.schema_matching_file, args.model_file1, args.model_file2, fst_graph, snd_graph)

        # Measure the execution time and write result as soon as possible
        # Prepare the GIG computation function call
        wrapped = wrapper(create_global_instance_graph, related_instances, fst_graph, snd_graph)
        computation_duration = timeit.repeat(wrapped, number=1, repeat=args.exec_nbr)
        times = ','.join(map(str, computation_duration))
        if not args.raw_output:
            with open(args.output_file, 'a') as f:
                f.write('{:d},{:s}\n'.format(total_duplicate_nbr, times))
        else:
            print('{:d},{:s}'.format(total_duplicate_nbr, times))

        total_duplicate_nbr += args.step_nbr
        if not args.raw_output:
            print(frmt.format(total_duplicate_nbr), end='')
    if not args.raw_output:
        print("\r[*] Benchmark done for {:d} couples of similar Customer instances. Result saved in {:s}.".format(args.limit_duplicate_nbr, args.output_file))
