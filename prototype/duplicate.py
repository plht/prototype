# -*- coding: utf-8 -*-
#
# Duplicate instances of one database into the other
# [!] Made for experimentation, this script does not adapt to new dataset.

import re
import sys
import time
import json
import string
import random
import datetime

import MySQLdb

from prototype import extract

def execute_query(args, query, values=None, get_pks=False, ex=False):
    """ """
    try:
        db = MySQLdb.connect(**args)
    except MySQLdb.Error as e:
        print("[!] Error while connecting to the database: {}".format(e), file=sys.stderr)

    result = None
    try:
        cursor = db.cursor(MySQLdb.cursors.DictCursor)
        if values is None:
            cursor.execute(query)
        elif get_pks:
            result = []
            for value in values:
                cursor.execute(query, value)
                result.append(cursor.lastrowid)
        else:
            cursor.executemany(query, values)

        if not get_pks:
            result = cursor.fetchall()
        db.commit()
    except MySQLdb.Error as e:
        db.rollback()
        print("[!] Error while issuing query to the database: {}\n\n[*] Query:\n{:s}".format(e, query), file=sys.stderr)
        if ex:
            raise e
    finally:
        cursor.close()
        db.close()
        if not result and not values:
            raise MySQLdb.Error()
    return result

def prepare_insert_query(table, instances):
    """ """
    query_frmt = "INSERT INTO {:s} ({:s}) VALUES ({:s})"
    prepare_frmt = ', '.join(['%s'] * len(instances[0]))
    return query_frmt.format(table, ', '.join(instances[0]), prepare_frmt)

def clean_bad_encoding(value):
    """
    Replace encoding related missing characters (identified by '?')
    with '%' to be able to search the city name efficiently
    """
    return value.replace('?', '%')

def random_date(n=1, is_datetime=False):
    """ """
    dates = []
    timestamp = random.randint(1, int(time.time()))
    for i in range(n if n > 0 else 1):
        if is_datetime:
            current_date = datetime.datetime.fromtimestamp(timestamp)
        else:
            current_date = datetime.date.fromtimestamp(timestamp)
        dates.append(current_date)
        timestamp += random.randint(1, int(time.time()))
    return dates

def fax_generator(fax_frmts):
    """ """
    frmt = random.choice(fax_frmts)
    # Find, from the formats, the size of the random integer
    sizes = map(int, re.findall(r'\d+', frmt))
    rands = [random.randrange(10 ** size) for size in sizes]
    return frmt.format(*rands)

def build_query_instances_related(model_file, attr_pk):
    """ """
    with open(model_file, 'r') as f:
        model = json.load(f)
    queries = [extract.build_query_dependencies_related_instances(link, {attr_pk: None}) for link in model['dependencies']]
    return queries

def query_sakila_customers_infos(args, duplicate_number, customer_avoid, model_file):
    """ """
    attributes = ['Customer.customer_id', 'Customer.first_name', 'Customer.last_name', 'Customer.create_date', 'Customer.address', 'Customer.phone', 'Customer.postal_code', 'Customer.city', 'Customer.country']
    attrs_as = map(lambda attr: "{:s} as '{:s}'".format(attr, attr), attributes)

    control_queries = build_query_instances_related(model_file, 'Customer.customer_id')
    full_sub_queries = map("Customer.customer_id IN ({:s})".format, control_queries)
    or_queries = '\nOR '.join(full_sub_queries)

    query = """SELECT {:s}
    FROM Customer
    WHERE {:s} """.format(', '.join(attrs_as), or_queries)
    if customer_avoid:
        query += "AND Customer.customer_id NOT IN ({:s}) ".format(', '.join(['%s'] * len(customer_avoid)))
    query += "ORDER BY RAND() LIMIT {:d}".format(duplicate_number)

    if customer_avoid:
        cas = tuple(map(str, customer_avoid))
        return execute_query(args, query, (cas,))
    else:
        return execute_query(args, query)

def query_contactTitles_regions(args, duplicate_number):
    """ """
    attributes = ['Customers.contactTitle', 'Customers.region']
    attrs_as = map(lambda attr: "{:s} as '{:s}'".format(attr, attr), attributes)
    query = """SELECT {:s}
    FROM Customers
    ORDER BY RAND()
    LIMIT {:d}
    """.format(', '.join(attrs_as), duplicate_number)
    return execute_query(args, query)

def query_existing_ship_infos(args, duplicate_number):
    """ """
    query_ship_infos = """SELECT shipName, shipAddress, shipCity, shipCountry, shipPostalCode, shipRegion, shipVia
    FROM Orders
    ORDER BY RAND()
    LIMIT {:d}""".format(duplicate_number)
    return execute_query(args, query_ship_infos)

def query_existing_products(args, duplicate_number):
    """ """
    query_product = """SELECT Products.productID
    FROM Products
    ORDER BY RAND()
    LIMIT {:d}""".format(duplicate_number)
    return execute_query(args, query_product)

def northwind_create_Customers(instances, fax_frmts, contactTitles_regions):
    """ """
    northwind_Customers_instances = [('contactName', 'contactTitle', 'address', 'region', 'phone', 'postalCode', 'fax', 'city', 'country', 'registrationDate')]
    for instance, contactTitle_region in zip(instances, contactTitles_regions):
        northwind_Customers_instances.append((
            instance['Customer.first_name'] + ' ' + instance['Customer.last_name'],
            contactTitle_region['Customers.contactTitle'],
            instance['Customer.address'],
            contactTitle_region['Customers.region'],
            instance['Customer.phone'],
            instance['Customer.postal_code'],
            fax_generator(fax_frmts),
            instance['Customer.city'],
            instance['Customer.country'],
            instance['Customer.create_date']))
    return northwind_Customers_instances

def northwind_create_Orders(customers_pks, customers, ship_infos):
    """ """
    northwind_Order_instances = [('customerID', 'employeeID', 'shipName', 'shipAddress', 'shipCity', 'shipCountry', 'shipPostalCode', 'shipRegion', 'shipVia', 'shippedDate', 'totalSales', 'amount', 'paymentDate', 'requiredDate', 'orderDate')]
    orderDate, paymentDate, shippedDate, requiredDate = random_date(4)
    for customers_pks, customer, s_i in zip(customers_pks, customers, ship_infos):
        northwind_Order_instances.append((
            customers_pks,
            random.randint(1, 9),
            *s_i.values(),
            shippedDate,
            random.uniform(1000.0, 5000.0),
            random.uniform(0.0, 2.0),
            paymentDate,
            requiredDate,
            orderDate))
    return northwind_Order_instances

def northwind_create_Order_Details(orders_pks, products):
    """ """
    northwind_Order_Details_instances = [('orderID', 'productID', 'quantity')]
    for product, order_pk in zip(products, orders_pks):
        northwind_Order_Details_instances.append((order_pk, product['productID'], random.randint(1, 100)))
    return northwind_Order_Details_instances

def insert_related_instance_into_Northwind(northwind_args, duplicate_number, insts_Customers, pks):
    """ """
    # Create new orders related with the duplicated customers
    ship_infos = query_existing_ship_infos(northwind_args, duplicate_number)
    nbr_si = len(ship_infos)
    if nbr_si < duplicate_number:
        # Not enought data, then fill space by copying existing one
        missing = duplicate_number - nbr_si
        ship_infos += tuple(random.choices(ship_infos, k=missing))

    insts_Orders = northwind_create_Orders(pks, insts_Customers[1:], ship_infos)
    query = prepare_insert_query('Orders', insts_Orders)
    orders_pks = execute_query(northwind_args, query, insts_Orders[1:], get_pks=True)

    # Create new order details related to the new orders
    products = query_existing_products(northwind_args, duplicate_number)
    nbr_pro = len(products)
    if nbr_pro < duplicate_number:
        # Not enought data, then fill space by copying existing one
        missing = duplicate_number - nbr_pro
        products += tuple(random.choices(products, k=missing))

    insts_Order_Details = northwind_create_Order_Details(orders_pks, products)
    query = prepare_insert_query('Order_Details', insts_Order_Details)
    order_details_pks = execute_query(northwind_args, query, insts_Order_Details[1:], get_pks=True)

    return orders_pks, order_details_pks

def duplicate_sakila_Customer_into_northwind_Customers(duplicate_number, sakila_args, northwind_args, model_file, customer_avoid=None):
    """ """
    # Duplicate random customers from sakila and format their data for northwind customers
    customer_infos = query_sakila_customers_infos(sakila_args, duplicate_number, customer_avoid, model_file)
    contactTitles_regions = query_contactTitles_regions(northwind_args, duplicate_number)
    fax_frmts = ["{:04d}-{:05d}", "({:03d}) {:03d}-{:04d}", "{:02d}.{:02d}.{:02d}.{:02d}"]
    insts_Customers = northwind_create_Customers(customer_infos, fax_frmts, contactTitles_regions)
    query = prepare_insert_query('Customers', insts_Customers)
    customers_pks = execute_query(northwind_args, query, insts_Customers[1:], get_pks=True)

    orders_pks, order_details_pks = insert_related_instance_into_Northwind(northwind_args, duplicate_number, insts_Customers, customers_pks)

    msg = """[*] Duplication of {:d} customer(s) from sakila to northwind successed !
    Primary keys of the customer in sakila:
      - Customer     : {}
    Primary keys of the new instances in northwind:
      - Customers    : {}
      - Orders       : {}
      - Order_Details: {}"""

    print(msg.format(duplicate_number, [inst['Customer.customer_id'] for inst in customer_infos], customers_pks, orders_pks, order_details_pks))
    return customers_pks

def query_northwind_customers_infos(args, duplicate_number, customers_avoid, model_file):
    """ """
    control_queries = build_query_instances_related(model_file, 'Customers.customerID')
    full_sub_queries = map("Customers.customerID IN ({:s})".format, control_queries)
    or_queries = ' OR '.join(full_sub_queries)
    
    query = "SELECT * FROM Customers WHERE {:s} ".format(or_queries)
    if customers_avoid:
        query += "AND Customers.customerID NOT IN ({:s}) ".format(', '.join(['%s'] * len(customers_avoid)))
    query += "ORDER BY RAND() LIMIT {:d}".format(duplicate_number)

    if customers_avoid:
        cas = tuple(map(str, customers_avoid))
        return execute_query(args, query, (cas,))
    else:
        return execute_query(args, query)

def query_film(args, duplicate_number):
    """ """
    query = "SELECT film_id FROM Film ORDER BY RAND() LIMIT {:d}".format(duplicate_number)
    return execute_query(args, query)

def sakila_create_Inventory(films_id):
    """ """
    sakila_Inventory_instances = [('film_id', 'store_id')]
    for film_id in films_id:
        sakila_Inventory_instances.append((film_id['film_id'], 1))
    return sakila_Inventory_instances

def sakila_create_Rental(customers_pks, inventories_pk):
    """ """
    sakila_Rental_instances = [('customer_id', 'inventory_id', 'staff_id', 'return_date', 'rental_date')]
    for customers_pk, inventory_pk in zip(customers_pks, inventories_pk):
        return_date, rental_date = random_date(2, is_datetime=True)
        sakila_Rental_instances.append((
            customers_pk,
            inventory_pk,
            1,
            return_date,
            rental_date))
    return sakila_Rental_instances

def sakila_create_Payment(customers_pks, rentals_pk):
    """ """
    sakila_Payment_instances = [('customer_id', 'rental_id', 'staff_id', 'amount', 'payment_date', 'total_sales')]
    for customers_pk, rental_pk in zip(customers_pks, rentals_pk):
        sakila_Payment_instances.append((
            customers_pk,
            rental_pk,
            1,
            random.randint(1, 10),
            random_date(is_datetime=True),
            random.uniform(550.0, 1000.0)))
    return sakila_Payment_instances

def sakila_create_Customer(customers):
    """ """
    sakila_customer_instances = [('store_id', 'first_name', 'last_name', 'email', 'active', 'address', 'city', 'country', 'district', 'postal_code', 'phone', 'create_date')]
    for customer in customers:
        first_name, last_name = customer['contactName'].split(' ', 1)
        sakila_customer_instances.append((
            1,
            first_name,
            last_name,
            "{:s}.{:s}@sakilacustomer.org".format(first_name, last_name),
            True,
            customer['address'],
            customer['city'],
            customer['country'],
            customer['region'],
            customer['postalCode'],
            customer['phone'],
            customer['registrationDate']))
    return sakila_customer_instances

def insert_related_instance_into_Sakila(northwind_args, duplicate_number, pks):
    """ """
    films_pk = query_film(northwind_args, duplicate_number)
    nbr_flm = len(films_pk)
    if nbr_flm < duplicate_number:
        # Not enought data, then fill space by copying existing one
        missing = duplicate_number - nbr_flm
        films_pk += tuple(random.choices(films_pk, k=missing))

    insts_Inventory = sakila_create_Inventory(films_pk)
    query = prepare_insert_query('Inventory', insts_Inventory)
    inventories_pk = execute_query(northwind_args, query, insts_Inventory[1:], get_pks=True, ex=True)

    insts_Rental = sakila_create_Rental(pks, inventories_pk)
    query = prepare_insert_query('Rental', insts_Rental)
    rentals_pk = execute_query(northwind_args, query, insts_Rental[1:], get_pks=True, ex=True)

    insts_Payment = sakila_create_Payment(pks, rentals_pk)
    query = prepare_insert_query('Payment', insts_Payment)
    payments_pk = execute_query(northwind_args, query, insts_Payment[1:], get_pks=True, ex=True)

    return inventories_pk, rentals_pk, payments_pk

def duplicate_northwind_Customers_into_sakila_Customer(duplicate_number, sakila_args, northwind_args, model_file, customer_avoid=None):
    """ """
    customer_infos = query_northwind_customers_infos(northwind_args, duplicate_number, customer_avoid, model_file)
    insts_Customer = sakila_create_Customer(customer_infos)
    query = prepare_insert_query('Customer', insts_Customer)
    customers_pks = execute_query(sakila_args, query, insts_Customer[1:], get_pks=True)

    inventories_pk, rentals_pk, payments_pk = insert_related_instance_into_Sakila(sakila_args, duplicate_number, customers_pks)

    msg = """[*] Duplication of {:d} customer(s) from northwind to sakila successed !
    Primary keys of the customer in northwind:
      - Customers     : {}
    Primary keys of the new instances in sakila:
      - Customer : {}
      - Inventory: {}
      - Rental   : {}
      - Payment  : {}"""

    customers_orig = [inst['customerID'] for inst in customer_infos]
    print(msg.format(duplicate_number, customers_orig, customers_pks, inventories_pk, rentals_pk, payments_pk))
    return customers_pks

def duplicate_from_sakila(sakila_args, northwind_args, duplicate_number, model_file):
    """ """
    random.seed(time.time())
    duplicate_sakila_Customer_into_northwind_Customers(duplicate_number, sakila_args, northwind_args, model_file)

def duplicate_from_northwind(sakila_args, northwind_args, duplicate_number, model_file):
    """ """
    random.seed(time.time())
    duplicate_northwind_Customers_into_sakila_Customer(duplicate_number, sakila_args, northwind_args, model_file)

def duplicate_from_both(sakila_args, northwind_args, duplicate_number, model_file1, model_file2):
    """ """
    random.seed(time.time())
    customers_avoid = duplicate_sakila_Customer_into_northwind_Customers(duplicate_number, sakila_args, northwind_args, model_file1)
    duplicate_northwind_Customers_into_sakila_Customer(duplicate_number, sakila_args, northwind_args, model_file2, customers_avoid)

def create_random_Customer_instances(nbr_insts):
    """ """
    fax_frmts = ["{:04d}-{:05d}", "({:03d}) {:03d}-{:04d}", "{:02d}.{:02d}.{:02d}.{:02d}"]
    northwind_Customers_instances = [('contactName', 'contactTitle', 'address', 'region', 'phone', 'postalCode', 'fax', 'city', 'country', 'registrationDate')]

    sakila_Customer_instances = [('store_id', 'first_name', 'last_name', 'email', 'active', 'address', 'city', 'country', 'district', 'postal_code', 'phone', 'create_date')]

    for _ in range(nbr_insts):
        first_name = ''.join(random.choices(string.ascii_lowercase, k=10))
        last_name = ''.join(random.choices(string.ascii_lowercase, k=10))
        contactTitle = ''.join(random.choices(string.ascii_lowercase, k=15))
        address = ''.join(random.choices(string.ascii_lowercase + string.digits, k=10))
        region = ''.join(random.choices(string.ascii_uppercase, k=5))
        phone = ''.join(random.choices(string.digits, k=10))
        postal_code = ''.join(random.choices(string.digits, k=10))
        city = ''.join(random.choices(string.ascii_lowercase, k=10))
        country = ''.join(random.choices(string.ascii_lowercase, k=10))
        d = random.randint(1, int(time.time()))
        create_date = datetime.datetime.fromtimestamp(d).strftime('%Y-%m-%d')

        northwind_Customers_instances.append((first_name + ' ' + last_name,
                contactTitle,
                address,
                region,
                phone,
                postal_code,
                fax_generator(fax_frmts),
                city,
                country,
                create_date))

        sakila_Customer_instances.append((1,
                first_name,
                last_name,
                 "{:s}.{:s}@sakilacustomer.org".format(first_name, last_name),
                True,
                address,
                city,
                country,
                region,
                postal_code,
                phone,
                create_date))

    return northwind_Customers_instances, sakila_Customer_instances

def add_duplicate_Customers_instances(duplicate_number, sakila_args, northwind_args):
    """ """
    random.seed(time.time())

    northwind_insts_Customers, sakila_insts_Customer = create_random_Customer_instances(duplicate_number)

    # Create duplicate customer for the nothix2 database
    query = prepare_insert_query('Customers', northwind_insts_Customers)
    customers_pks_snd = execute_query(northwind_args, query, northwind_insts_Customers[1:], get_pks=True, ex=True)

    orders_pks, order_details_pks = insert_related_instance_into_Northwind(northwind_args, duplicate_number, northwind_insts_Customers, customers_pks_snd)

    len_customers = len(customers_pks_snd)
    len_orders = len(orders_pks)
    len_order_details = len(order_details_pks)
    assert len_customers == len_orders and len_orders == len_order_details, "Not enought instance create for the customers of Northix2"

    # Create duplicate customer for the sakila database
    query = prepare_insert_query('Customer', sakila_insts_Customer)
    customers_pks_fst = execute_query(sakila_args, query, sakila_insts_Customer[1:], get_pks=True, ex=True)

    assert len(customers_pks_fst) == len(customers_pks_snd), "Two lists of customers doesn't have the same size !"

    insts = ['{:s} - {:s}'.format(str(i), str(j)) for i, j in zip(customers_pks_fst, customers_pks_snd)]
    with open('debug_inst_similar.csv', 'w') as f:
        f.write(';\n'.join(insts))

    inventories_pk, rentals_pk, payments_pk = insert_related_instance_into_Sakila(sakila_args, duplicate_number, customers_pks_fst)

    len_customers = len(customers_pks_fst)
    len_inventories = len(inventories_pk)
    len_rentals = len(rentals_pk)
    len_payement = len(payments_pk)

    assert len_customers == len_inventories and len_inventories == len_rentals and len_rentals == len_payement, "Not enought instance create for the customers of Northix1"
