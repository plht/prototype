# -*- coding: utf-8 -*-
#
# Link two graphs into a global graph by
# finding the similar instances betwen graphs

import os
import json
import base64

from bitarray import bitarray

def extract_related_attributes(model_file, schema_mapping):
    """
    Extract from the attributes of the model related to
    the schema matching computed in the Northix dataset
    """
    with open(model_file, 'r') as f:
        model = json.load(f)

    dep_links = []
    for link in model['dependencies']:
        for key in ['from', 'to']:
            dep_links.append("{:s}.{:s}".format(model['name'], link[key]['attribute']))
    dep_links = set(dep_links)

    rel = {}
    for depl in dep_links:
        for mapping in schema_mapping:
            if depl in mapping:
                if mapping not in rel:
                    rel[mapping] = []
                rel[mapping].append(depl)
    return rel

def extract_dependency_attributes_matching(schema_mapping_file, model_1, model_2):
    """Extract dependency attributes related to the schema matching"""
    frmt_schema_matching = lambda x: "{bdd:s}.{table:s}.{attr:s}".format_map(x)
    with open(schema_mapping_file) as f:
        schema_mapping = [tuple(map(frmt_schema_matching, sm)) for sm in json.load(f)]

    bd1_rel = extract_related_attributes(model_1, schema_mapping)
    bd2_rel = extract_related_attributes(model_2, schema_mapping)

    keys = bd1_rel.keys() & bd2_rel.keys()
    return [bd1_rel[key] + bd2_rel[key] for key in keys]

def extract_matching_edges(graph, match):
    """Extract edges related to the maching attributes"""
    matching_edges = []
    for edge in graph['edges']:
        for node in edge.values():
            table, _, attribute = node
            table_attr = "{:s}.{:s}.{:s}".format(graph['name'], table, attribute)
            if table_attr in match:
                matching_edges.append(tuple(node))
    return set(matching_edges)

def deserialise_bloom_filter(bytes_data):
    """Deserialise Bloom Filter from base64 to bitarray"""
    bf = bitarray(endian='big')
    data = base64.b64decode(bytes_data.encode(encoding='utf-8', errors='strict'))
    bf.frombytes(data)
    return bf

def compute_dice_coefficient(bf1, bf2):
    """
    Compute the similarity of two Bloom Filters
    using the Dice coefficient score
    """
    fst_bf = deserialise_bloom_filter(bf1)
    snd_bf = deserialise_bloom_filter(bf2)
    a = fst_bf.count()
    b = snd_bf.count()
    h = (fst_bf & snd_bf).count()
    return (2 * h) / (a + b)

def get_related_instances(schema_mapping_file, model_file1, model_file2, fst_graph, snd_graph):
    """Get the graph that are related to the model attribute matching"""
    related_instances = {}
    related_matching = extract_dependency_attributes_matching(schema_mapping_file, model_file1, model_file2)
    for match in related_matching:
        fst = extract_matching_edges(fst_graph, match)
        snd = extract_matching_edges(snd_graph, match)
        related_instances[tuple(match)] = (fst, snd)
    return related_instances

def create_global_instance_graph(related_instances, fst_graph, snd_graph):
    """
    Compute the pairwise instances similarity and collect
    couples of instances which are above the similarity threshold
    """
    matching_instances = []
    similarity_treshold = 0.6
    for match, (fst, snd) in related_instances.items():
        for node_fst in fst:
            table_fst, pk_fst, attr_fst = node_fst
            fst_bf = fst_graph['tables'][table_fst][pk_fst]['bfs']
            for node_snd in snd:
                table_snd, pk_snd, attr_snd = node_snd
                snd_bf = snd_graph['tables'][table_snd][pk_snd]['bfs']

                dice = compute_dice_coefficient(fst_bf, snd_bf)
                if dice > similarity_treshold:
                    matching_instances.append({'from': node_fst, 'to': node_snd})
    return matching_instances

def generate_global_graph(schema_mapping_file, model_file1, model_file2, graph_file1, graph_file2, global_graph_file):
    """Generate the global graph which unify all the graphs"""

    with open(graph_file1, 'r') as f:
        fst_graph = json.load(f)

    with open(graph_file2, 'r') as f:
        snd_graph = json.load(f)

    related_instances = get_related_instances(schema_mapping_file, model_file1, model_file2, fst_graph, snd_graph)
    matching_instances = create_global_instance_graph(related_instances, fst_graph, snd_graph)

    with open(global_graph_file, 'w') as f:
        json.dump({'edges': fst_graph['edges'] + snd_graph['edges'], 'matches': matching_instances}, f)
