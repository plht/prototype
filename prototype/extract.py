#!/usr/bin/python3.6
# -*- coding: utf8 -*-
#
# Use the model to extract the related instances
# that will be anonymised and encoded into Bloom Filters

import os
import sys
import json

import MySQLdb
import MySQLdb.cursors

def build_query_dependencies_related_instances(link, pks):
    """Build the query used to fetch the related instances pks"""
    access = map(lambda a: "{:s} = {:s}".format(*a), link['access'])
    tables = ', '.join(link['tables'])
    ands = ' AND '.join(access)
    pks_joined = ', '.join(pks.keys())
    return "SELECT {:s} FROM {:s} WHERE {:s}".format(pks_joined, tables, ands)

def build_query_values_instances(table, pk, pks):
    """Build the query used to fetch the attributes value of instances"""
    pks_prep = ', '.join(['%s'] * len(pks))
    return "SELECT * FROM {:s} WHERE {:s} IN ({:s})".format(table, pk, pks_prep)

def build_query_show_columns(table):
    """Build a query used to get the attributes of a table"""
    return "SHOW COLUMNS FROM {:s}".format(table)

def execute_query(args, query, value=None, cursor_type=None):
    """Execute a given query"""
    try:
        db = MySQLdb.connect(**args)
    except MySQLdb.Error as e:
        print("[!] Error while connecting to the database: {}".format(e), file=sys.stderr)
        raise e

    result = None
    ex_rollback = None
    try:
        cursor = db.cursor(cursor_type)
        if value:
            cursor.execute(query, value)
        else:
            cursor.execute(query)
        db.commit()
        result = cursor.fetchall()
    except MySQLdb.Error as e:
        db.rollback()
        ex_rollback = e
    finally:
        cursor.close()
        db.close()
        if not result:
            raise MySQLdb.Error()
        if ex_rollback != None:
            raise ex_rollback
    return result 

def fetch_related_instances_pk(args, model):
    """Fetch the pks of the instances related to the model dependencies"""
    instances_pk = {}
    related_instances_pk = []
    for link in model['dependencies']:
        current_pks = {}
        for key in ['from', 'to']:
            end = link[key]
            table = end['table']
            pk = model['tables'][table]['pk']
            current_pks[pk] = table
        end_attrs = (link['from']['attribute'], link['to']['attribute'])
        query = build_query_dependencies_related_instances(link, current_pks)
        instances_pks = execute_query(args, query)
        related_instances_pk.append((end_attrs,) + instances_pks)
        count = 0
        for _, table in current_pks.items():
            if table in instances_pk:
                instances_pk[table] |= set(value[count] for value in instances_pks)
            else:
                instances_pk[table] = set(value[count] for value in instances_pks)
            count += 1
    return instances_pk, related_instances_pk

def fetch_columns_fields(args, table):
    """Fetch the columns fields of a table"""
    query = build_query_show_columns(table)
    columns_infos = execute_query(args, query, cursor_type=MySQLdb.cursors.DictCursor)
    return tuple(column['Field'] for column in columns_infos)

def fetch_related_instances_values(args, model, instances_pk):
    """Fetch the attributes value of instances"""
    instances_values = {}
    for table, pks in instances_pk.items():
        pk = model['tables'][table]['pk']
        query = build_query_values_instances(table, pk, pks)
        instances = execute_query(args, query, pks)
        if instances:
            columns_fields = fetch_columns_fields(args, table) 
            instances_values[table] = (columns_fields,) + instances
    return instances_values

def extract_instances(model_file, host, name, user, password):
    """
    Use the information of the model to
    extract related instances from a database
    """
    with open(model_file, 'r') as f:
        model = json.load(f)

    args = {'host': host, 'database': name, 'user': user, 'password': password}
    instances_pk, related_instances_pk = fetch_related_instances_pk(args, model)
    instances_values = fetch_related_instances_values(args, model, instances_pk)
    return instances_values, related_instances_pk

def build_pairs(attrs, schema_mapping):
    """Build all the pairs of matching attributes"""
    length = len(attrs)
    attr_name = lambda attr: attr[0].lower() + attr[1:]
    # Handle the fact that the following tables
    # have been merge into Customer in '1.dat' files
    table_in_customer = ['address', 'city', 'country']
    conv_name_db = {'1.dat': 'sakila', '2.txt': 'northwind'}
    for i in range(length):
        attr_i, table_i, db_i = attrs[i].split('@')
        if table_i in table_in_customer:
            table_i = 'customer'
        for j in range(i):
            attr_j, table_j, db_j = attrs[j].split('@')
            if table_j in table_in_customer:
                table_j = 'customer'
            if db_i != db_j:
                schema_mapping.append((
                    {'bdd': conv_name_db[db_j], 'table': table_j.title(), 'attr': attr_name(attr_j)},
                    {'bdd': conv_name_db[db_i], 'table': table_i.title(), 'attr': attr_name(attr_i)}
                ))

def extract_dependency_attributes_matching(mapping_dir, output):
    """Extract the schema matching"""
    schema_mapping = []
    for d in os.listdir(mapping_dir):
        if d == 'UNCLASSED':
            continue
        sub_dir = os.path.join(mapping_dir, d)
        if os.path.isdir(sub_dir):
            build_pairs(os.listdir(sub_dir), schema_mapping)

    with open(output, 'w') as f:
        json.dump(schema_mapping, f, indent=4)

