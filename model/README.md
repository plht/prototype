# Semantic Inference Model (SIM)

A SIM represent the inference between attributes of a database (i.e., between attributes of the same table or between attributes of tables related by a primary-foreign key relation).

The SIM have been proposed by [Chen, Y., & Chu, W. W. (2006, May)](https://www.researchgate.net/profile/Wesley_Chu/publication/243135169_Database_Security_Protection_Via_Inference_Detection/links/543580c30cf2643ab986799c/Database-Security-Protection-Via-Inference-Detection.pdf) and use three kind of links:

- *Schema link*: represent primary-foreign key relation between two tables
- *Dependency link*: represent probabilistic relation which are based on the information present in the database
- *Semantic link*: represent domain specific knowledge (i.e., relation that cannot be detected by computing probabilistic table)

The SIM is based on the _Probabilistic Relational Model_ which can either be created manualy by an operator or learned by using an algorithm such as the one presented in [Friedman, N., Getoor, L., Koller, D., & Pfeffer, A. (1999, July)](http://ai.stanford.edu/people/nir/Papers/FGKP1.pdf).

# JSON model representation

In this project, the SIM is represented as a .json file and called `model` in the source code.

The file `sim_schema.json` define the schema of the model using the vocabulary of [JSON Schema](https://json-schema.org/).

A tool such as [jsonschema](https://github.com/Julian/jsonschema) can be used to validate a model with the schema. For example: `jsonschema sim_schema.json -i 1/model.json` where `model.json` is the SIM to validate.

# Experimentation

The following picture depicted the SIMs and the related schema matching relations used for the measure of the GIG computation time.

![SIMs of the Sakila (left) database and Northwind (right) database, respectively. With the related schema matching relations extracted from the Northix dataset.](exp_models.png)
