-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 08, 2019 at 11:20 AM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `northwind`
--

-- --------------------------------------------------------

--
-- Table structure for table `Customers`
--

CREATE TABLE `Customers` (
  `customerID` int(11) NOT NULL,
  `contactName` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `contactTitle` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `address` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `city` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `country` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `fax` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  `phone` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  `postalCode` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  `region` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  `registrationDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `Orders`
--

CREATE TABLE `Orders` (
  `orderID` int(11) NOT NULL,
  `customerID` int(11) NOT NULL,
  `employeeID` int(11) NOT NULL,
  `shipName` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `shipAddress` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `shipCity` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `shipCountry` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `shipPostalCode` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  `shipRegion` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  `shipVia` int(11) NOT NULL,
  `shippedDate` date NOT NULL,
  `totalSales` float NOT NULL,
  `amount` float NOT NULL,
  `paymentDate` date NOT NULL,
  `requiredDate` date NOT NULL,
  `orderDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `Order_Details`
--

CREATE TABLE `Order_Details` (
  `odID` int(11) NOT NULL,
  `orderID` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `Products`
--

CREATE TABLE `Products` (
  `productID` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL,
  `supplierID` int(11) NOT NULL,
  `productName` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `quantityPerUnit` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `unitsInStock` int(11) NOT NULL,
  `unitsOnOrder` int(11) DEFAULT NULL,
  `reorderLevel` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `Suppliers`
--

CREATE TABLE `Suppliers` (
  `supplierID` int(11) NOT NULL,
  `contactTitle` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `address` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `city` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `country` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `postalCode` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `region` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  `fax` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  `phone` varchar(1024) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Customers`
--
ALTER TABLE `Customers`
  ADD PRIMARY KEY (`customerID`);

--
-- Indexes for table `Orders`
--
ALTER TABLE `Orders`
  ADD PRIMARY KEY (`orderID`),
  ADD KEY `customerID` (`customerID`),
  ADD KEY `employeeID` (`employeeID`);

--
-- Indexes for table `Order_Details`
--
ALTER TABLE `Order_Details`
  ADD PRIMARY KEY (`odID`),
  ADD KEY `orderID` (`orderID`),
  ADD KEY `productID` (`productID`);

--
-- Indexes for table `Products`
--
ALTER TABLE `Products`
  ADD PRIMARY KEY (`productID`),
  ADD KEY `supplierID` (`supplierID`);

--
-- Indexes for table `Suppliers`
--
ALTER TABLE `Suppliers`
  ADD PRIMARY KEY (`supplierID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Customers`
--
ALTER TABLE `Customers`
  MODIFY `customerID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Orders`
--
ALTER TABLE `Orders`
  MODIFY `orderID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Order_Details`
--
ALTER TABLE `Order_Details`
  MODIFY `odID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Products`
--
ALTER TABLE `Products`
  MODIFY `productID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Suppliers`
--
ALTER TABLE `Suppliers`
  MODIFY `supplierID` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Orders`
--
ALTER TABLE `Orders`
  ADD CONSTRAINT `Orders_ibfk_1` FOREIGN KEY (`customerID`) REFERENCES `Customers` (`customerID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Order_Details`
--
ALTER TABLE `Order_Details`
  ADD CONSTRAINT `Order_Details_ibfk_1` FOREIGN KEY (`orderID`) REFERENCES `Orders` (`orderID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Order_Details_ibfk_2` FOREIGN KEY (`productID`) REFERENCES `Products` (`productID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Products`
--
ALTER TABLE `Products`
  ADD CONSTRAINT `Products_ibfk_1` FOREIGN KEY (`supplierID`) REFERENCES `Suppliers` (`supplierID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
