# Source

Dataset: [Northix](https://archive.ics.uci.edu/ml/datasets/Northix)

# Schema modifications

The following modification have been made to the databases schema.

## Sakila

The similarity score computed with the _Bloom Filters_ for customer instances were too low even when they were known to be similar. Thus, attributes of the following tables: `Address`, `City` and `Country` in _Sakila_ are merged into the table `Customer` in order to be closer to the `Customers` schema in _Northwind_. As depicted by the following picture.

![Initial schema of the tables Address, City and Country of the Sakila database.](tables.png)

## Northwind

The primary key of `Customers` table in the _Northwind_ database have been modified from `varchar(5)` to `int(11) AUTO_INCREMENT` in order to avoid generating existing primary keys while inserting randomly created instances of customers.

# Sakila database schema

![Sakila Schema 1](sakila.png)

# Northwind database schema

![Northwind Schema 2](northwind.png)
