FROM python:3.6-slim

COPY . /prototype
WORKDIR /prototype

# To use MySQLdb Python library and to compile all the Python libraries
RUN apt-get update && apt-get install -y default-libmysqlclient-dev build-essential

# Remove libraries related to graph visualization
RUN pip install --upgrade pip && pip install --no-cache-dir $(grep -ivE 'networkx|seaborn' requirements.txt)
