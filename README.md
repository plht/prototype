# Prototype

Prototype which demonstrate the fesability of the _Semantic Instance Graphs_ (SIGs) linking algorithm to compute the _Global Instance Graph_ (GIG) in order to detect distributed inference attacks. Where a SIG is obtained by instanciating a _Semantic Inference Model_ (SIM) with instances of a database. This solution extend the work presented in [Chen, Y., & Chu, W. W. (2006, May)](https://www.researchgate.net/profile/Wesley_Chu/publication/243135169_Database_Security_Protection_Via_Inference_Detection/links/543580c30cf2643ab986799c/Database-Security-Protection-Via-Inference-Detection.pdf)

## Project description

- The [dataset/](./dataset), [graph/](./graph) and [model/](./model) folders have their own descriptions as a local README.md file.
- prototype/: the project is structured as a standard Python package. *extract.py*, *anonymise.py* and *build.py* contains programs which process the SIM of a database to generate the SIG and the _Bloom Filters_ (BFs) of the related instances.
  - *extract.py* : extracts from a database the instances related to the dependencies of a SIM.
  - *anonymise.py*: anonymise the extracted instances and create the BFs.
  - *build.py*: build the SIG related to the anonymised instances.
  - *link.py*: contains the implementation of the GIG computation algorithm.
  - *benchmark.py*: measure the computation time of the GIG.
  - *load.py*: load the two database of the dataset into a MySQL DBSM.
  - *duplicate.py*: insert similar instances in the databases. Either by copying existing customer instances from one database to another one or by generating random data used to create a pair of customer instances. 
  - *visualise.py*: visualise a computed GIG. Rely on the following Python libraries: `networkx` and `seaborn`.
  - *\_\_main\_\_.py*: expose the API of the prototype as a CLI. See the [usage](#usage) section below.

## Tutorials

The first tutorial cover all the sub-commands of the prototype to show [how to compute a GIG](./tutorial#basic-interaction-to-compute-the-global-instance-graph-gig).

The second one show how to use the benchmark programm to [reproduce the measures](./tutorial#reproduce-the-measures).

## Usage

Enter `python3 -m prototype -h` to display the sub-commands.

Enter `python3 -m prototype <sub-command> -h` to display the options of a sub-command.

### Load Sub-command

Load the [Northix](https://archive.ics.uci.edu/ml/datasets/Northix) dataset in two MySQL databases.

###### Example

The command: `python3 -m prototype load dataset/Northix/ localhost root test sakila dataset/sakila.sql northwind dataset/northwind.sql`

* Get the Northix dataset root path saved in `dataset/Northix/`
* Connect to the MySQL DBMS hosted in `localhost` using the user `root` and the password `test`
* Create the first database called `sakila` and use the schema `dataset/sakila.sql`
* Create the second database called `northwind` and use the schema `dataset/northwind.sql`

### Build Sub-command

Build the anonymised SIG and the Bloom Filters (BFs) related to the instances of the database.

###### Example

The command: `python3 -m prototype build model/sakila.json localhost sakila root test graph/sakila.json`:

* Use the SIM defined in `model/sakila.json`
* Extract the related instances from the database `localhost/sakila` using the user `root` and the password `test`
* Anonymise extracted instances into a SIG and compute their respective BF
* Save the anonymised SIG and the BFs into `graph/sakila.json`

### Duplicate Sub-command

Duplicate the customers instances of one database into the other.

###### Example

The command: `python3 -m prototype duplicate localhost sakila northwind root test 2 both`

* Duplicate `2` customers instances from `both` databases and insert them into the opposite database
* The first database is hosted in `localhost`, called `sakila` and can be queried using the user `root` with the password `test`. The second database is called `northwind`, is hosted like the first one and can be queried with the same user.

### Extract matching Sub-command

Extract the schema matching from the Northix dataset.

###### Example

The command: `python3 -m prototype extract_matching dataset/Northix/ model/schema_matching.json`

* Get the Northix dataset root path saved in `dataset/Northix/`
* Save the schema matching into `model/schema_matching.json`

### Link Sub-command

Link the SIGs together in order to obtain the GIG.

###### Example

The command: `python3 -m prototype link model/schema_matching.json model/sakila.json graph/sakila.json model/northwind.json graph/northwind.json graph/global_graph.json`

* Use the file `model/schema_matching.json` representing the schema matching of the Northix dataset
* Use the SIMs `model/sakila.json` and `model/northwind.json` to extract dependencies attributes related to the schema mapping
* Use the SIGs `graph/sakila.json` and `graph/northwind.json` to link graphs according to the similar instances
* Produce the GIG `graph/global_graph.json`

### Visualise Sub-command

Visualise the GIG.

###### Example

The command: `python3 -m prototype visualise graph/global_graph.json model/sakila.json model/northwind.json`

* Use the GIG `graph_global_graph.json`
* And the SIMs `model/sakila.json` and `model/northwind.json`
* Display the GIG using dependencies related color

### Benchmark Sub-command

Measure the evolution of the GIG computation time for an increasing amount of pairs of similar instances.

###### Example

The command: `python3 -m prototype benchmark dataset/Northix/ localhost root test sakila dataset/sakila.sql northwind dataset/northwind.sql model/1/sakila.json model/1/northwind.json graph/sakila.json graph/northwind.json graph/global_graph.json model/1/schema_matching.json 100 10000 10 ./results.csv`

* Load `dataset/Northix` into a MySQL DBMS
* Insert `100` pairs of randomly created and similar customer instances
* Build the SIGs of the SIMs `model/1/sakila.json` and `model/1/northwind.json`, respectively `graph/sakila.json` and `graph/northwind.json`
* Repeat the GIG computation `10` times
* Measure each execution and write the times in `./results.csv`
* Until `10000` pairs have been inserted, repeat the process from the second item

## Information

Developed with Python version `3.6.8` and `phphMyAdmin 4.6.6`.
